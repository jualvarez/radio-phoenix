import enum

from radio_phoenix.base.events import BaseEvent


class SelectDirection(enum.Enum):
    RIGHT = "right"
    LEFT = "left"


class SelectorActivatedEvent(BaseEvent):
    direction: SelectDirection
