from base.debugger import BaseDebugger
from base.player import BasePlayerBackend, PlayerNotDefinedException
from selector.events import SelectorActivatedEvent


event_commands = {
    SelectorActivatedEvent: [
        'debugger.event_debugger',
        'player.change_station'
    ]
}

__current_player = None


def set_player(player: BasePlayerBackend):
    global __current_player
    __current_player = player


def get_current_player() -> BasePlayerBackend:
    if not isinstance(__current_player, BasePlayerBackend):
        raise PlayerNotDefinedException
    return __current_player


__debugger = BaseDebugger()


def get_current_debugger() -> BaseDebugger:
    return __debugger
