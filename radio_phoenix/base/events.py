import uuid

import config
from pydantic import BaseModel


class BaseEvent(BaseModel):
    id: uuid.UUID = None

    def __init__(self, **data):
        data["id"] = uuid.uuid4()
        super().__init__(**data)

    def __hash__(self):
        return self.id


class EventHandler:
    def trigger(self, event):
        from config import event_commands
        methods_to_call = event_commands[type(event)]
        component_map = {
            "debugger": config.get_current_debugger(),
            "player": config.get_current_player()
        }

        for method_reference in methods_to_call:
            component, method_name = method_reference.split(".")
            method_to_call = getattr(component_map[component], method_name)
            method_to_call(event)


handler = EventHandler()
