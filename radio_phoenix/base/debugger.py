import logging

logger = logging.getLogger(__name__)


class BaseDebugger:
    @staticmethod
    def event_debugger(event):
        logger.info(f"Processed event {event}")
