import abc

from base.events import BaseEvent
from pydantic import BaseModel


class PlayerStation(BaseModel):
    pass


class PlayerSong(BaseModel):
    pass


class PlayerQueue(BaseModel):
    pass


class BasePlayerBackend(BaseModel, metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def change_station(self, event: BaseEvent):
        pass


class PlayerNotDefinedException(ValueError):
    pass
