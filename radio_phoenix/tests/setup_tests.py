import unittest

import config
from base.player import PlayerNotDefinedException


class SetUpTestCase(unittest.TestCase):
    def test_getting_player_without_setting_raisesException(self):
        with self.assertRaises(PlayerNotDefinedException):
            config.get_current_player()


if __name__ == '__main__':
    unittest.main()
