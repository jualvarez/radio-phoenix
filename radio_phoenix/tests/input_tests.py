import unittest
from unittest import mock

import config
from base import events, player
from selector.events import SelectDirection, SelectorActivatedEvent


class SelectorTestCase(unittest.TestCase):
    def test_selector_up_event_changes_station(self):
        mock_player = mock.Mock(spec=player.BasePlayerBackend)
        config.set_player(mock_player)
        next_event = SelectorActivatedEvent(direction=SelectDirection.RIGHT)
        events.handler.trigger(next_event)
        mock_player.change_station.assert_called()


if __name__ == "__main__":
    unittest.main()
